#!/bin/bash

# Pass env-variables to configs
sed -i 's/_nginx_port/'"$NGINX_PORT"'/' '/etc/nginx/conf.d/default.conf'
sed -i 's/_haproxy_back_port_1/'"$HAPROXY_BACK_PORT_1"'/' '/etc/haproxy/haproxy.cfg'
sed -i 's/_haproxy_back_port_2/'"$HAPROXY_BACK_PORT_2"'/' '/etc/haproxy/haproxy.cfg'
sed -i 's/_haproxy_front_port/'"$HAPROXY_FRONT_PORT"'/' '/etc/haproxy/haproxy.cfg'

# Start HAPROXY process
/usr/sbin/haproxy -f "/etc/haproxy/haproxy.cfg" &
status=$?
if [ $status -ne 0 ]; then
  echo "Failed to start HAPROXY: $status"
  exit $status
fi

# Start NGINX process
/usr/sbin/nginx -g "daemon off; pid /tmp/nginx.pid;" &
status=$?
if [ $status -ne 0 ]; then
  echo "Failed to start NGINX: $status"
  exit $status
fi

# Naive check runs checks once a minute to see if either of the processes exited.
# This illustrates part of the heavy lifting you need to do if you want to run
# more than one service in a container. The container exits with an error
# if it detects that either of the processes has exited.
# Otherwise it loops forever, waking up every 60 seconds

while sleep 10; do
  ps aux |grep nginx |grep -q -v grep
  NGINX_STATUS=$?
  ps aux |grep haproxy |grep -q -v grep
  HAPROXY_STATUS=$?
  # If the greps above find anything, they exit with 0 status
  # If they are not both 0, then something is wrong
  if [ $NGINX_STATUS -ne 0 -o $HAPROXY_STATUS -ne 0 ]; then
    echo "One of the processes has already exited."
    exit 1
  fi
done