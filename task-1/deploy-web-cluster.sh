#!/bin/bash

# Deploy two docker containers
docker build -t nginx-haproxy-container:latest . -f Dockerfile
docker run -p 80:80 -d -e NGINX_PORT='8080' -e HAPROXY_BACK_PORT_1='8080' -e HAPROXY_BACK_PORT_2='8081' -e HAPROXY_FRONT_PORT='80' --network="host" nginx-haproxy-container:latest 
docker run -p 81:81 -d -e NGINX_PORT='8081' -e HAPROXY_BACK_PORT_1='8080' -e HAPROXY_BACK_PORT_2='8081' -e HAPROXY_FRONT_PORT='81' --network="host" nginx-haproxy-container:latest 

# Check if HTTP-response is 200
if [ $(curl --write-out "%{http_code}\n" --silent --output /dev/null "localhost:80") -ne 200 ]; then
  echo "Haproxy's Port 80 Is In Not Listening State"
  something_wrong=true
fi
if [ $(curl --write-out "%{http_code}\n" --silent --output /dev/null "localhost:81") -ne 200 ]; then
  echo "Haproxy's Port 81 Is In Not Listening State"
  something_wrong=true
fi
if $something_wrong ; then
    exit 1
fi
