# Задача 1 #

Развернуть web-кластер из двух docker-машин с помощью одного bash-скрипта.

<ul>
<li>Среда контейнеризации: Docker 19.x</li>
<li>Операционная система: Linux</li>
<li>Балансировщик: HAProxy - any</li>
<li>Веб-сервер: Nginx - any</li>
</ul>

Написать bash-скрипт, который в существующей среде контейнеризации Docker создаст и запустит два docker-образа операционной системы Linux, установит в каждый из образов балансировщик HAProxy и веб-сервер Nginx, настроит балансировщики таким образом, чтобы трафик шел на один из доступных веб-серверов Nginx. Bash-скрипт после запуска docker-образов операционной системы Ubuntu, должен проверить доступность веб-серверов Nginx, используя порты балансировщиков HAProxy.

***

## Description ##

Hi, guys.

### How It Works? ###
#### deploy-web-cluster.sh ####

Initial script to run to deploy web-cluster. It creates docker image based on Dockerfile. Dockerfile describes alpine image as a core Linux and Nginx, Haproxy installed. I chose this approach to reduce size of the image. Also, each image contains `haproxy.cfg` and `nginx.conf`, as well as `start-apps.sh` as startup script.

```console
docker build -t nginx-haproxy-container:latest . -f Dockerfile
```

I use environment variables to pass Nginx and Haproxy configuration during container creation.

```console
docker run -p 80:80 -d -e NGINX_PORT='8080' -e HAPROXY_BACK_PORT_1='8080' -e HAPROXY_BACK_PORT_2='8081' -e HAPROXY_FRONT_PORT='80' --network="host" nginx-haproxy-container:latest 
docker run -p 81:81 -d -e NGINX_PORT='8081' -e HAPROXY_BACK_PORT_1='8080' -e HAPROXY_BACK_PORT_2='8081' -e HAPROXY_FRONT_PORT='81' --network="host" nginx-haproxy-container:latest 
```

Finally, script checks if HTTP-response is 200 on Haproxy port for both containers.

#### start-apps.sh ####

Script `start-apps.sh` will substitude values every container's start.
```console
sed -i 's/_nginx_port/'"$NGINX_PORT"'/' '/etc/nginx/conf.d/default.conf'
sed -i 's/_haproxy_back_port_1/'"$HAPROXY_BACK_PORT_1"'/' '/etc/haproxy/haproxy.cfg'
sed -i 's/_haproxy_back_port_2/'"$HAPROXY_BACK_PORT_2"'/' '/etc/haproxy/haproxy.cfg'
sed -i 's/_haproxy_front_port/'"$HAPROXY_FRONT_PORT"'/' '/etc/haproxy/haproxy.cfg'
```

It's necessary because the configuration files have templated format with variables to replace, e.g. have a look on `haproxy.cfg` below:
```console
frontend myfrontend
        bind :_haproxy_front_port
        default_backend mybackend

backend mybackend
        balance roundrobin
        server s1 127.0.0.1:_haproxy_back_port_1 check
        server s2 127.0.0.1:_haproxy_back_port_2 check
```

Also, script runs our application and perform checks to see if either of the processes exited. The container exits with an error if it detects that either of the processes has exited.

### How To Run? ###
I've created EC2 instance like a sandbox to play. It has Docker installed. You can connect by SSH in order to check this out:

```console
wget https://gist.githubusercontent.com/souvlakee/b5813ebf187b5c524bcfa978b87a9c8a/raw/f86931b18cb8939618588742130b31cb71b5c97b/max-keys.pem
chmod 400 max-keys.pem
ssh -i max-keys.pem centos@3.8.236.174
```
And run:

```console
cd /home/centos/exness/task-1/ 
./deploy-web-cluster.sh
```

After script is finished, you can access web-cluster using URLs:
```console
3.8.236.174:80
3.8.236.174:81
```