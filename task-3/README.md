# Задача 3 #

Развернуть kubernetes-кластер.

<ul>
<li>Среда контейнеризации: K3s or GKE or EKS</li>
<li>Операционная система: Linux</li>
<li>Веб-сервер: Nginx - any, Backend: PHP</li>
<li>Балансировщик: any</li>
</ul>

Написать скрипт, который развернет среду контейнеризации, запустит в ней веб-сервер Nginx и настроит бекенд Php для приема запросов.  Скрипт после запуска веб-сервера и бекенда, должен проверить их доступность , используя порты балансировщика.

***

## Description ##

Hi, guys.

### How To Run? ###
I've created EC2 instance like a sandbox to play. It has Ansible, AWS CLI, Docker installed. You can connect by SSH in order to check this out:

```console
wget https://gist.githubusercontent.com/souvlakee/b5813ebf187b5c524bcfa978b87a9c8a/raw/f86931b18cb8939618588742130b31cb71b5c97b/max-keys.pem
chmod 400 max-keys.pem
ssh -i max-keys.pem centos@3.8.236.174
```
And run:

```console
ansible-playbook /home/centos/exness/task-3/deploy-eks-cluster.yml
```

After script is finished, you will see web-cluster ELB load balancer URL like on an example below:
```console
TASK [Show Load Balancer Entry Point] ***************************************************************************************************************************************************************************
ok: [localhost] => {
    "msg": "The Load Balancer URL: http://nginx-els-load-balancer-1252172012.eu-west-2.elb.amazonaws.com"
}
```
