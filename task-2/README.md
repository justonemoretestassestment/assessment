# Задача 2 #

Развернуть web-кластер из двух EC2 хостов.

<ul>
<li>Среда контейнеризации: ECS</li>
<li>Операционная система: Linux</li>
<li>Балансировщик: Elastic Load Balancer</li>
<li>Веб-сервер: Nginx - any</li>
</ul>

Написать скрипт, который в существующей среде контейнеризации создаст и запустит два docker-образа операционной системы Linux, установит в каждый образ веб-сервер Nginx, настроит балансировщик таким образом, чтобы трафик шел на один из доступных веб-серверов Nginx. Скрипт после запуска docker-образов операционной системы Ubuntu, должен проверить доступность веб-серверов Nginx, используя порты балансировщика.

***

## Description ##

Hi, guys.

**Carolina from HR Dept. told me not to do this task because it's replaced to another one**.

However, I decided to make it done anyway in a slightly different manner using Ansible, not script language. Ansible is a more appropriate tool to provision AWS infrastructure and performs necessary checks.

To reduce the size of the image I decided to use alpine docker Nginx image instead of Linux docker with installed Nginx.

### How To Run? ###
I've created EC2 instance like a sandbox to play. It has Ansible, AWS CLI, Docker installed. You can connect by SSH in order to check this out:

```console
wget https://gist.githubusercontent.com/souvlakee/b5813ebf187b5c524bcfa978b87a9c8a/raw/f86931b18cb8939618588742130b31cb71b5c97b/max-keys.pem
chmod 400 max-keys.pem
ssh -i max-keys.pem centos@3.8.236.174
```
And run (using my test credentials):

```console
ansible-playbook /home/centos/exness/task-2/deploy-web-cluster.yml -e '{
    "aws_access_key":"AKIAJXPIQU54LKZ7RJKQ", 
    "aws_secret_key":"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",
    "region":"eu-west-2",
    "ecr_repo_name":"nginx-docker-container",
    "ec2_sec_group_name":"nginx-security-group",
    "ec2_elb_lb_name":"nginx-els-load-balancer",
    "ec2_instance_name":"nginx-ec2-instance",
    "ecs_taskdefinition_name":"nginx-container-task-definition",
    "ecs_taskdefinition_container_name":"nginx-container"
}'
```

On the latest task you'll see Elastic Load Balancer url wich points to EC2 instances under ECS:

```console
TASK [Show Load Balancer Entry Point] ***************************************************************************************************************************************************************************
ok: [localhost] => {
    "msg": "The Load Balancer URL: http://nginx-els-load-balancer-1252172012.eu-west-2.elb.amazonaws.com"
}
```
