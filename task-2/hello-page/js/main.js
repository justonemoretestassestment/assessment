  $(document).ready(function() {    
    
    $('.title').delay(2000).fadeIn(900, function(){
      setTimeout(function(){
        $('.title').fadeOut('slow');
      }, 8000);
    });

    $('#speech').delay(6000).fadeIn(900, function(){
      setTimeout(function(){
        $('#speech').fadeOut('slow');
      }, 1000);
    });

    $('#rocket').delay(7500).fadeIn(2000);
    $('#car').delay(8200).fadeOut(500);

});